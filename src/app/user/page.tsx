'use client';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Select from 'react-select';
import { useRouter } from 'next/navigation';

interface User {
    id: number | null;
    name: string;
    email: string;
    roles: { id: number; name: string }[];
}

const UserPage: React.FC = () => {
    const [users, setUsers] = useState<User[]>([]);
    const [showModal, setShowModal] = useState<boolean>(false);
    const [currentUser, setCurrentUser] = useState<User>({ id: null, name: '', email: '', roles: [] });
    const [isEditMode, setIsEditMode] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');
    const [roleOptions, setRoleOptions] = useState<{ value: number; label: string }[]>([]);
    const router = useRouter();

    useEffect(() => {
        const storedToken = localStorage.getItem('token');
        if (storedToken) {
            setToken(storedToken);
        } else {
            router.push('/auth');
        }
    }, []);

    useEffect(() => {
        if (token) {
            const fetchRoles = async () => {
                try {
                    const response = await axios.get<{ id: number; name: string }[]>('http://localhost:3000/roles', {
                        headers: { Authorization: `Bearer ${token}` }
                    });
                    const options = response.data.map(role => ({ value: role.id, label: role.name }));
                    setRoleOptions(options);
                } catch (error) {
                    console.error('Failed to fetch roles:', error);
                }
            };

            fetchRoles();
        }
    }, [token]);

    useEffect(() => {
        if (token) {
            const fetchUsers = async () => {
                try {
                    const response = await axios.get<User[]>('http://localhost:3000/users', {
                        headers: { Authorization: `Bearer ${token}` }
                    });
                    setUsers(response.data);
                } catch (error) {
                    console.error('Failed to fetch users:', error);
                }
            };

            fetchUsers();
        }
    }, [token]);

    const handleShowModal = (user?: User) => {
        setCurrentUser(user || { id: null, name: '', email: '', roles: [] });
        setIsEditMode(!!user?.id);
        setShowModal(true);
    };

    const handleRoleChange = (selectedRoles: any) => {
        setCurrentUser({ ...currentUser, roles: selectedRoles.map((role: any) => ({ id: role.value, name: role.label })) });
    };

    const handleCloseModal = () => setShowModal(false);

    const handleSaveUser = async (event: React.FormEvent) => {
        event.preventDefault();
        try {
            if (isEditMode) {
                const response = await axios.put<User>(`http://localhost:3000/users/${currentUser.id}`, currentUser, {
                    headers: { Authorization: `Bearer ${token}` }
                });
                setUsers(users.map(user => (user.id === currentUser.id ? response.data : user)));
            } else {
                const response = await axios.post<User>('http://localhost:3000/users', currentUser, {
                    headers: { Authorization: `Bearer ${token}` }
                });
                setUsers([...users, response.data]);
            }
            handleCloseModal();
        } catch (error) {
            console.error('Failed to save user:', error);
        }
    };

    const handleDeleteUser = async (id: number | null) => {
        try {
            if (!id) return;
            await axios.delete(`http://localhost:3000/users/${id}`, {
                headers: { Authorization: `Bearer ${token}` }
            });
            setUsers(users.filter(user => user.id !== id));
        } catch (error) {
            console.error('Failed to delete user:', error);
        }
    };

    return (
        <div className="container mt-5">
            <h2 className="mb-4">User Management</h2>
            <button className="btn btn-primary" onClick={() => handleShowModal()}>Add User</button>
            <table className="table table-striped table-bordered table-hover mt-3">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map(user => (
                        <tr key={user.id || 0}>
                            <td>{user.id}</td>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td>{(user.roles || []).map(role => role.name).join(', ')}</td>
                            <td>
                                <button className="btn btn-warning me-2" onClick={() => handleShowModal(user)}>Edit</button>
                                <button className="btn btn-danger" onClick={() => handleDeleteUser(user.id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {showModal && (
                <div className="modal show d-block" tabIndex={-1} role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">{isEditMode ? 'Edit User' : 'Add User'}</h5>
                            </div>
                            <form onSubmit={handleSaveUser}>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label htmlFor="name">Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="name"
                                            value={currentUser.name}
                                            onChange={(e) => setCurrentUser({ ...currentUser, name: e.target.value })}
                                            required
                                        />
                                    </div>
                                    <div className="form-group mt-3">
                                        <label htmlFor="email">Email address</label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            id="email"
                                            value={currentUser.email}
                                            onChange={(e) => setCurrentUser({ ...currentUser, email: e.target.value })}
                                            required
                                        />
                                    </div>
                                    <div className="form-group mt-3">
                                        <label htmlFor="roles">Roles</label>
                                        <Select
                                            id="roles"
                                            isMulti
                                            options={roleOptions}
                                            value={roleOptions.filter(option => currentUser.roles.some(role => role.id === option.value))}
                                            onChange={handleRoleChange}
                                        />
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" onClick={handleCloseModal}>Close</button>
                                    <button type="submit" className="btn btn-primary">{isEditMode ? 'Save Changes' : 'Add User'}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default UserPage;

